using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using musichino.Commands;
using musichino.Data.Models;
using musichino.Services;
using Xunit;
using static musichino.Services.MessageService;

namespace Tests
{
    public partial class MessageServiceTest
    {
        [Fact]
        public async Task PerformActionTest_ShouldAddArtist()
        {
            // Given
            const int EXTERNAL_USER_ID = 1;
            var expectedArtist = TestHelper.ArtistModelFactory(true);
            var options = TestHelper.OptionsFactory("add_user_artist_db");
            var expectedUser = new UserModel()
            {
                Id = new Guid(),
                ExternalId = EXTERNAL_USER_ID,
                CreatedAtUtc = DateTime.UtcNow,
                IsActive = true
            };

            using (var context = new MusichinoDbContext(options))
            {
                // When
                var service = TestHelper.MessageServiceFactory(context);
                await context.Artists.AddAsync(expectedArtist);
                await context.Users.AddAsync(expectedUser);
                await context.SaveChangesAsync();
                var messageText = "add " + expectedArtist.Id.ToString();
                var message = new MessageCommand() {
                    Text = messageText,
                    ExternalUserId = EXTERNAL_USER_ID
                };

                await service.PerformAction(Commands.Add, expectedUser.Id, message);
                var actualUser = await context.Users.FirstOrDefaultAsync();
                var actualArtist = await context.Artists.FirstOrDefaultAsync();
                var actualArtistUser = await context.ArtistUsers.FirstOrDefaultAsync();

                // Then
                Assert.NotEmpty(context.Artists);
                Assert.NotNull(actualArtist);
                Assert.NotNull(actualArtistUser);
                Assert.Equal(expectedArtist.Id, actualArtist.Id);
                Assert.Equal(expectedUser.Id, actualArtistUser.UserId);
            }
        }

        [Fact]
        public async Task PerformActionTest_ShouldSuspendUserActivity()
        {
            // Given
            var options = TestHelper.OptionsFactory("suspend_user_db");
            var expectedUser = new UserModel()
            {
                Id = new Guid(),
                ExternalId = 1,
                CreatedAtUtc = DateTime.UtcNow,
                IsActive = true
            };

            using (var context = new MusichinoDbContext(options))
            {
                var service = TestHelper.MessageServiceFactory(context);
                await context.Users.AddAsync(expectedUser);
                await context.SaveChangesAsync();

                // When
                await service.PerformAction(Commands.Suspend, expectedUser.Id, null);
                var actualUser = await context.Users.FirstOrDefaultAsync(u => u.Id == expectedUser.Id);

                // Then
                Assert.NotNull(actualUser);
                Assert.Equal(actualUser.IsActive, false);
            }
        }

        [Fact]
        public async Task PerformActionTest_ShouldReactivateUserActivity()
        {
            // Given
            var options = TestHelper.OptionsFactory("reactivate_user_db");
            var expectedUser = new UserModel()
            {
                Id = new Guid(),
                ExternalId = 1,
                CreatedAtUtc = DateTime.UtcNow,
                IsActive = false
            };

            using (var context = new MusichinoDbContext(options))
            {
                var service = TestHelper.MessageServiceFactory(context);
                await context.Users.AddAsync(expectedUser);
                await context.SaveChangesAsync();

                // When
                await service.PerformAction(Commands.Reactivate, expectedUser.Id, null);
                var actualUser = await context.Users.FirstOrDefaultAsync(u => u.Id == expectedUser.Id);

                // Then
                Assert.NotNull(actualUser);
                Assert.Equal(actualUser.IsActive, true);
            }
        }
    }
}