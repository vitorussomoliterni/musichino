using System;
using Microsoft.EntityFrameworkCore;
using musichino.Data.Models;
using musichino.Services;
using musichino.Commands;

namespace Tests
{
    internal class TestHelper
    {
        internal static DbContextOptions<MusichinoDbContext> OptionsFactory(string dbName)
        {
            var options = new DbContextOptionsBuilder<MusichinoDbContext>()
                .UseInMemoryDatabase(dbName)
                .Options;

            return options;
        }

        internal static MessageService MessageServiceFactory(MusichinoDbContext context)
        {
                var userService = new UserService(context);
                var musicbrainzService = new MusicbrainzService();
                var artistService = new ArtistService(context, musicbrainzService);
                var messageService = new MessageService(userService, musicbrainzService, artistService);

                return messageService;
        }

        internal static string RawMessageFactory(string userMessage)
        {
            var rawMessage = @"{'update_id': 10000,
                                'message': {
                                    'date': 1483228800,
                                       'chat': {
                                            'last_name': 'Test Lastname',
                                            'id': 1111111,
                                            'first_name': 'Test',
                                            'username': 'Test'
                                            },
                                        'message_id': 1365,
                                        'from': {
                                            'last_name': 'Test Lastname',
                                            'id': 1111111,
                                            'first_name': 'Test',
                                            'username': 'Test'
                                            },
                                        'text':'" + userMessage + 
                                    @"'}
                                }";
            return rawMessage;
        }


        internal static MessageCommand MessageModelFactory(string text)
        {
            var messageModel = new MessageCommand()
            {
                MessageId = 1365,
                ExternalUserId = 1111111,
                UtcDate = new DateTime(2017, 1, 1),
                LastName = "Test Lastname",
                FirstName = "Test",
                Username = "Test",
                Text = text
            };

            return messageModel;
        }

        internal static ArtistModel ArtistModelFactory(bool shouldIncludeId)
        {
            var artist = new ArtistModel() {
                Name = "nofx",
                Type = "punk",
                Country = "USA",
                BeginYear = "1983",
                Ended = false
            };

            if (shouldIncludeId)
            {
                artist.Id = new Guid();
            }

            return artist;
        }
    }
}