using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Xunit;
using musichino.Data.Models;
using musichino.Services;
using Moq;
using System.Linq;
using System.Collections.Generic;

namespace Tests
{
    public class ArtistServiceTests
    {
        [Fact]
        public async void TestGetArtist_ShouldReturnArtistModel()
        {
            var options = TestHelper.OptionsFactory("get_artst_db");
            using (var context = new MusichinoDbContext(options))
            {
                // Given
                var expectedArtist = TestHelper.ArtistModelFactory(true);
                var artistService = new ArtistService(context, null);
                await context.Artists.AddAsync(expectedArtist);
                await context.SaveChangesAsync();

                // When
                var actualArtist = await artistService.GetArtist(expectedArtist.Id);

                // Then
                Assert.NotNull(actualArtist);
                Assert.Equal(expectedArtist.Id, actualArtist.Id);
                Assert.Equal(expectedArtist.Name, actualArtist.Name);
                Assert.Equal(expectedArtist.Type, actualArtist.Type);
                Assert.Equal(expectedArtist.Country, actualArtist.Country);
                Assert.Equal(expectedArtist.BeginYear, actualArtist.BeginYear);
                Assert.Equal(expectedArtist.Ended, actualArtist.Ended);
            }
        }

        [Fact]
        public async void TestAddArtist_ShouldAddArtistToDb()
        {
            var options = TestHelper.OptionsFactory("add_artist_db");
            using (var context = new MusichinoDbContext(options))
            {
                // Given
                var expextedArtist = TestHelper.ArtistModelFactory(false);
                var artistService = new ArtistService(context, null);

                // When
                await artistService.AddArtist(expextedArtist);
                var actualArtist = await context.Artists.FirstOrDefaultAsync();

                // Then
                Assert.NotNull(actualArtist);
                Assert.Equal(expextedArtist.Id, actualArtist.Id);
                Assert.Equal(expextedArtist.Name, actualArtist.Name);
                Assert.Equal(expextedArtist.Type, actualArtist.Type);
                Assert.Equal(expextedArtist.Country, actualArtist.Country);
                Assert.Equal(expextedArtist.BeginYear, actualArtist.BeginYear);
                Assert.Equal(expextedArtist.Ended, actualArtist.Ended);
            }
        }

        [Fact]
        public async Task TestRemoveArtist_ShouldRemoveArtistFromUserDb()
        {
            var options = TestHelper.OptionsFactory("remove_artist_db");
            using (var context = new MusichinoDbContext(options))
            {
                // Given
                var expectedArtist = TestHelper.ArtistModelFactory(true);
                var text = $"remove {expectedArtist.Name}";
                var message = TestHelper.MessageModelFactory(text);
                var expectedUser = new UserModel()
                {
                    Id = new Guid(),
                    FirstName = "Steven",
                    LastName = "Universe",
                    Username = "Zucchini",
                    ExternalId = 1,
                    CreatedAtUtc = DateTime.UtcNow,
                    IsActive = true
                };
                var artistService = new ArtistService(context, null);
                var expectedArtistUser = new ArtistUserModel()
                {
                    ArtistUserId = new Guid(),
                    ArtistId = expectedArtist.Id,
                    Artist = expectedArtist,
                    UserId = expectedUser.Id,
                    User = expectedUser
                };
                await context.Users.AddAsync(expectedUser);
                await context.Artists.AddAsync(expectedArtist);
                await context.ArtistUsers.AddAsync(expectedArtistUser);
                await context.SaveChangesAsync();
                
                // When
                var actualArtist = await artistService.RemoveArtist(expectedUser.Id, message);

                // Then
                Assert.Empty(context.ArtistUsers);
                Assert.Equal(expectedArtist.Id, actualArtist.Id);
            }
        }

        [Fact]
        public void TestGetArtistNameFromMessage_ShouldReturnArtistName()
        {
            // Given
            const string EXPECTED_ARTIST_NAME = "nofx";
            const string MESSAGE_TEXT = "add " + EXPECTED_ARTIST_NAME;
            var options = TestHelper.OptionsFactory("test_get_artist_db");

            using (var context = new MusichinoDbContext(options))
            {
                var message = TestHelper.MessageModelFactory(MESSAGE_TEXT);
                var service = new ArtistService(context, null);
                
                // When
                var actualArtistName = service.GetArtistDetailsFromMessage(message);

                // Then
                Assert.Equal(EXPECTED_ARTIST_NAME, actualArtistName);
            }
        }

        [Fact]
        public async void TestGetArtists_ShouldReturnArtistList()
        {
            var options = TestHelper.OptionsFactory("test_get_artists_db");

            using (var context = new MusichinoDbContext(options))
            {
                // Given
                var artistService = new ArtistService(context, null);
                var user = new UserModel()
                {
                    Id = new Guid(),
                    FirstName = "Steven",
                    LastName = "Universe",
                    Username = "Zucchini",
                    ExternalId = 1,
                    CreatedAtUtc = DateTime.UtcNow,
                    IsActive = true
                };
                var artistModel = TestHelper.ArtistModelFactory(false);
                var expectedArtistList = new List<ArtistModel>();
                expectedArtistList.Add(artistModel);
                context.Artists.Add(artistModel);
                context.Users.Add(user);
                context.ArtistUsers.Add(new ArtistUserModel() {
                    Artist = artistModel,
                    User = user
                });
                await context.SaveChangesAsync();
                            
                // When
                var actualArtistList = await artistService.GetArtists(user.Id);
                
                // Then
                Assert.NotNull(actualArtistList);
                Assert.NotEmpty(actualArtistList);
                Assert.Equal(expectedArtistList, actualArtistList);
            }
        }
    }
}