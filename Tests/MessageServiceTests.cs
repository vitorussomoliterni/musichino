using System;
using Xunit;
using musichino.Commands;
using musichino.Services;
using System.Threading.Tasks;
using System.Text;
using System.IO;
using musichino.Data.Models;

namespace Tests
{
    public partial class MessageServiceTests
    {
        [Fact]
        public void TestGetMessageCommand_ShouldReturnAdd()
        {
            const string MESSAGE_TEXT = "add nofx";
            var options = TestHelper.OptionsFactory("add_command_db");
            
            using (var context = new MusichinoDbContext(options))
            {
                // Given
                var service = TestHelper.MessageServiceFactory(context);
                var rawMessage = TestHelper.RawMessageFactory(MESSAGE_TEXT);
                var messageModel = TestHelper.MessageModelFactory(MESSAGE_TEXT);

                //When
                var actualText = service.GetMessageCommand(messageModel.Text);
                var expectedText = MessageService.Commands.Add;

                //Then
                Assert.Equal(expectedText, actualText);
            }
        }

        [Fact]
        public void TestGetMessageCommand_ShouldReturnRemove()
        {
            const string MESSAGE_TEXT = "remove green day";
            var options = TestHelper.OptionsFactory("remove_command_db");
            
            using (var context = new MusichinoDbContext(options))
            {
                //Given
                var service = TestHelper.MessageServiceFactory(context);
                var rawMessage = TestHelper.RawMessageFactory(MESSAGE_TEXT);
                var messageModel = TestHelper.MessageModelFactory(MESSAGE_TEXT);

                //When
                var actualText = service.GetMessageCommand(messageModel.Text);
                var expectedText = MessageService.Commands.Remove;

                //Then
                Assert.Equal(expectedText, actualText);
            }
        }
        
        [Fact]
        public void TestGetMessageCommand_ShouldReturnSuspend()
        {
            const string MESSAGE_TEXT = "suspend";
            var options = TestHelper.OptionsFactory("suspend_command_db");
            
            using (var context = new MusichinoDbContext(options))
            {
                //Given
                var service = TestHelper.MessageServiceFactory(context);
                var rawMessage = TestHelper.RawMessageFactory(MESSAGE_TEXT);
                var messageModel = TestHelper.MessageModelFactory(MESSAGE_TEXT);

                //When
                var actualText = service.GetMessageCommand(messageModel.Text);
                var expectedText = MessageService.Commands.Suspend;

                //Then
                Assert.Equal(expectedText, actualText);
            }
        }

        [Fact]
        public void TestGetMessageCommand_ShouldReturnHelp()
        {
            var options = TestHelper.OptionsFactory("help_command_db");
            const string MESSAGE_TEXT = "help";
            
            using (var context = new MusichinoDbContext(options))
            {
                //Given
                var service = TestHelper.MessageServiceFactory(context);
                var rawMessage = TestHelper.RawMessageFactory(MESSAGE_TEXT);
                var messageModel = TestHelper.MessageModelFactory(MESSAGE_TEXT);

                //When
                var actualText = service.GetMessageCommand(messageModel.Text);
                var expectedText = MessageService.Commands.Help;

                //Then
                Assert.Equal(expectedText, actualText);
            }
        }

        [Fact]
        public void TestGetMessageCommand_ShouldReturnList()
        {
            const string MESSAGE_TEXT = "list";
            var options = TestHelper.OptionsFactory("list_command_db");
            
            using (var context = new MusichinoDbContext(options))
            {
                //Given
                var service = TestHelper.MessageServiceFactory(context);
                var rawMessage = TestHelper.RawMessageFactory(MESSAGE_TEXT);
                var messageModel = TestHelper.MessageModelFactory(MESSAGE_TEXT);

                //When
                var actualText = service.GetMessageCommand(messageModel.Text);
                var expectedText = MessageService.Commands.List;

                //Then
                Assert.Equal(expectedText, actualText);
            }
        }

        [Fact]
        public void TestGetMessageCommand_ShouldReturnReactivate()
        {
            const string MESSAGE_TEXT = "reactivate";
            var options = TestHelper.OptionsFactory("reactivate_command_db");
            
            using (var context = new MusichinoDbContext(options))
            {
                //Given
                var service = TestHelper.MessageServiceFactory(context);
                var rawMessage = TestHelper.RawMessageFactory(MESSAGE_TEXT);
                var messageModel = TestHelper.MessageModelFactory(MESSAGE_TEXT);

                //When
                var actualText = service.GetMessageCommand(messageModel.Text);
                var expectedText = MessageService.Commands.Reactivate;

                //Then
                Assert.Equal(expectedText, actualText);
            }
        }

        [Fact]
        public void TestGetMessageCommand_ShouldThrowInvalidDataException()
        {
            var options = TestHelper.OptionsFactory("invalid_data_exception_db");
            
            using (var context = new MusichinoDbContext(options))
            {
                //Given
                var service = TestHelper.MessageServiceFactory(context);

                //Then
                Assert.Throws<InvalidDataException>(() => service.GetMessageCommand("   "));
                Assert.Throws<InvalidDataException>(() => service.GetMessageCommand(""));
                Assert.Throws<InvalidDataException>(() => service.GetMessageCommand(null));
                Assert.Throws<InvalidDataException>(() => service.GetMessageCommand(" "));
            }
        }

        [Fact]
        public void TestGetMessageCommand_ShouldReturnOther()
        {
            const string MESSAGE_TEXT = "avada kevadra";
            var options = TestHelper.OptionsFactory("other_command_db");
            
            using (var context = new MusichinoDbContext(options))
            {
                //Given
                var service = TestHelper.MessageServiceFactory(context);
                var rawMessage = TestHelper.RawMessageFactory(MESSAGE_TEXT);
                var messageModel = TestHelper.MessageModelFactory(MESSAGE_TEXT);

                //When
                var actualText = service.GetMessageCommand(messageModel.Text);
                var expectedText = MessageService.Commands.Other;

                //Then
                Assert.Equal(expectedText, actualText);
            }
        }

        [Fact]
        public void TestGetMessage_ShouldGetTheMessageText()
        {
            const string MESSAGE_TEXT = "add nofx";
            var options = TestHelper.OptionsFactory("get_message)_text_db");
            
            using (var context = new MusichinoDbContext(options))
            {
                //Given
                var service = TestHelper.MessageServiceFactory(context);
                var rawMessage = TestHelper.RawMessageFactory(MESSAGE_TEXT);
                var messageModel = TestHelper.MessageModelFactory(MESSAGE_TEXT);

                //When
                var actualMessage = service.GetMessage(rawMessage);
                var expectedMessage = messageModel;

                //Then
                Assert.NotNull(actualMessage);
                Assert.Equal(expectedMessage.UtcDate, actualMessage.UtcDate);
                Assert.Equal(expectedMessage.ExternalUserId, actualMessage.ExternalUserId);
                Assert.Equal(expectedMessage.FirstName, actualMessage.FirstName);
                Assert.Equal(expectedMessage.LastName, actualMessage.LastName);
                Assert.Equal(expectedMessage.MessageId, actualMessage.MessageId);
                Assert.Equal(expectedMessage.Text, actualMessage.Text);
                Assert.Equal(expectedMessage.Username, actualMessage.Username);
            }
        }

        // TODO: Add testing for reading the request
        [Fact]
        public async Task TestReadRequestBodyAsync_ShouldReturnString()
        {
            const string MESSAGE_TEXT = "add nofx";
            var options = TestHelper.OptionsFactory("read_request_db");
            
            using (var context = new MusichinoDbContext(options))
            {
                //Given
                var service = TestHelper.MessageServiceFactory(context);
                var rawMessage = TestHelper.RawMessageFactory(MESSAGE_TEXT);
                
                //When
                using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(rawMessage)))
                {
                    var result = await service.ReadRequestBodyAsync(stream);
                
                    //Then
                    Assert.Equal(rawMessage, result);
                }
            }
        }
    }
}