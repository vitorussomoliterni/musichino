﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using musichino.Services;
using musichino.Data.Models;
using musichino.Commands;

namespace musichino.Controllers
{
    [Route("")]
    public class WebHook : Controller
    {
        private IArtistRepository _artistRepository;
        private MessageService _message;
        private UserService _user;

        public WebHook(IArtistRepository artistRepository, MessageService message, UserService user)
        {
            _artistRepository = artistRepository;
            _message = message;
            _user = user;
        }
        
        [HttpPost("")]
        public async Task<IActionResult> ReceiveMessages()
        {
            try
            {
                var rawMessage = await _message.ReadRequestBodyAsync(Request.Body);
                var message = _message.GetMessage(rawMessage);
                var action = _message.GetMessageCommand(message.Text);
                var user = await _user.GetUser(message.ExternalUserId);
                
                if (user == null)
                {
                    user = await _user.AddUser(message);
                }

                await _message.PerformAction(action, user.Id, message);

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
