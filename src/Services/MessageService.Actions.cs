using System;
using System.Threading.Tasks;
using musichino.Commands;
using musichino.Data.Models;

namespace musichino.Services
{
    public partial class MessageService
    {
        UserService _user;
        IArtistRepository _artistRepository;
        ArtistService _artist;
        public MessageService(UserService user, IArtistRepository artistRepository, ArtistService artist)
        {
            _user = user;
            _artistRepository = artistRepository;
            _artist = artist;
        }
        public async Task PerformAction(Commands action, Guid userId, MessageCommand message)
        {
            switch (action)
            {
                case Commands.Add:
                    await addArtist(message);
                    break;
                case Commands.Search:
                    // Let's search
                    break;
                case Commands.Help:
                    // Let's help
                    break;
                case Commands.List:
                    // Let's list
                    break;
                case Commands.Other:
                    // Let's other
                    break;
                case Commands.Reactivate:
                    await _user.reactivateUser(userId);
                    break;
                case Commands.Remove:
                    await _artist.RemoveArtist(userId, message);
                    break;
                case Commands.Suspend:
                    await _user.suspendUser(userId);
                    break;
                default:
                    // How are you even here?
                    throw new InvalidOperationException("No action found");
            }
        }

        private async Task<ArtistUserModel> addArtist(MessageCommand message)
        {
            var artistId = new Guid(_artist.GetArtistDetailsFromMessage(message));
            var artist = await _artist.GetArtist(artistId);
            var user = await _user.GetUser(message.ExternalUserId);
            var artistModel = await _artist.AddArtistUser(artist, user);

            return artistModel;
        }
    }
}