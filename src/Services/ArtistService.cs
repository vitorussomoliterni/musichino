using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using musichino.Data.Models;
using musichino.Commands;

namespace musichino.Services
{
    public class ArtistService
    {
        MusichinoDbContext _context;
        IArtistRepository _artistRepository;
        public ArtistService(MusichinoDbContext context, IArtistRepository artistRepository)
        {
            _context = context;
            _artistRepository = artistRepository;
        }

        public async Task<ArtistUserModel> AddArtistUser(ArtistModel artist, UserModel user)
        {
            if(artist == null || user == null)
            {
                throw new ArgumentNullException("No artist or user provided");
            }

            var artistUser = await _context.ArtistUsers.FirstOrDefaultAsync(au => au.ArtistId == artist.Id && au.UserId == user.Id);

            if(artistUser != null)
            {
                throw new InvalidOperationException($"Artist {artist.Name} already being tracked");
            }

            artistUser = new ArtistUserModel() {
                ArtistUserId = new Guid(),
                Artist = artist,
                ArtistId = artist.Id,
                User = user,
                UserId = user.Id
            };

            await _context.ArtistUsers.AddAsync(artistUser);
            await _context.SaveChangesAsync();
            Log.Information($"Artist {artist.Id} added to user {user.Id}");

            return artistUser;
        }

        public async Task<ArtistModel> GetArtist(Guid? artistId)
        {
            if (artistId == null)
            {
                throw new ArgumentNullException("No artist id provided");
            }

            var artist = await _context.Artists.FirstOrDefaultAsync(a => a.Id == artistId);

            if (artist == null)
            {
                throw new InvalidOperationException($"No artist found for id {artistId}");
            }

            return artist;
        }

        public async Task<ArtistModel> AddArtist(ArtistModel artist)
        {
            if (artist == null)
            {
                throw new ArgumentNullException("Artist model cannot be null");
            }

            artist.Id = new Guid();
            var actualArtist = await _context.Artists.AddAsync(artist);
            await _context.SaveChangesAsync();
            Log.Information($"Artist {actualArtist.Entity.Id} added to the database");

            return actualArtist.Entity;
        }

        public async Task<ArtistModel> RemoveArtist(Guid userId, MessageCommand message)
        {
            if (message == null)
            {
                throw new ArgumentNullException("No message provided");
            }

            var user = await _context.Users.FirstOrDefaultAsync(u => u.Id == userId);

            if (user == null)
            {
                throw new NullReferenceException($"No user found for id {userId}");
            }

            var artistName = GetArtistDetailsFromMessage(message);
            
            var artistUser = await _context.ArtistUsers.FirstOrDefaultAsync(au => au.UserId == userId && au.Artist.Name == artistName);

            if (artistUser == null)
            {
                throw new NullReferenceException($"No artist named ${artistName} for user {userId}");
            }

            var artistToRemove = artistUser.Artist;

            _context.ArtistUsers.Remove(artistUser);
            await _context.SaveChangesAsync();

            return artistToRemove;
        }

        public async Task<List<ArtistModel>> GetArtists(Guid userId)
        {
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Id == userId);

            if (user == null)
            {
                throw new NullReferenceException($"No user found for id {userId}");
            }
            
            var artists = await _context.ArtistUsers
                .Where(au => au.UserId == userId)
                .Select(a => a.Artist)
                .ToListAsync();

            if(artists == null)
            {
                throw new NullReferenceException($"No artists for user {userId}");
            }

            return artists;
        }

        private async Task<List<ArtistModel>> searchArtist(string message, string artistName)
        {
            var artists = await _artistRepository.GetArtistNameList(artistName);

            return artists.ToList();
        }

        public String GetArtistDetailsFromMessage(MessageCommand message)
        {
            var text = message.Text;
            var indexOfSpace = text.Trim().IndexOf(" ");
            var artistName = text.Substring(indexOfSpace).Trim();

            return artistName;
        }
    }
}