﻿using System.Threading.Tasks;
using musichino.Data.Models;
using System.Collections.Generic;

namespace musichino.Services
{
    public interface IArtistRepository
    {
        Task<IEnumerable<ArtistModel>> GetArtistNameList(string artistName);
    }
}